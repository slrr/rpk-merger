﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace RPKMerger
{
  public partial class MainWindow : Window
  {
    private ObservableCollection<Model.RpkToMerge> rpks;
    private ObservableCollection<string> messages;
    private SlrrLib.Model.HighLevel.GameFileManager gManag = null;
    private SlrrLib.Model.HighLevel.RpkMerger merger = null;
    private string lastDir = "";
    private Task currentOperation = null;
    private bool operationCalced = false;

    private string LastDirForDialog
    {
      get
      {
        if (lastDir == "" && System.IO.File.Exists("lastDir"))
        {
          lastDir = System.IO.File.ReadAllText("lastDir");
        }
        else if(lastDir == "")
        {
          LastDirForDialog = System.IO.Directory.GetCurrentDirectory();
        }
        return lastDir;
      }
      set
      {
        System.IO.File.WriteAllText("lastDir", value);
        lastDir = value;
      }
    }

    public MainWindow()
    {
      //SCXFile.ClassJavaPair test = new SCXFile.ClassJavaPair(@"F:\PlayGround\Slrr_clean\SlrrVanilla\cars\allracer\baiern\scripts\Baiern_VT.class");
      //test.BinaryClassTUFAEntries.First();
      SlrrLib.Model.MessageLog.ErrorAdded += messageLog_ErrorAdded;
      SlrrLib.Model.MessageLog.MessageAdded += messageLog_MessageAdded;
      messages = new ObservableCollection<string>();
      InitializeComponent();
      rpks = new ObservableCollection<Model.RpkToMerge>();
      ctrlListRPK.ItemsSource = rpks;
      ctrlListMessages.ItemsSource = messages;
      currentOperation = new Task(() =>
      {
      });
      currentOperation.Start();
      System.IO.File.WriteAllText("logOfMerge.txt", "");
      messages.CollectionChanged += messages_CollectionChanged;
    }

    private void messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      foreach (var newItem in e.NewItems)
      {
        System.IO.File.AppendAllText("logOfMerge.txt", newItem.ToString()+"\n");
      }
      ctrlListMessages.ScrollIntoView(messages.Last());
    }
    private void messageLog_MessageAdded(SlrrLib.Model.Message e)
    {
      Dispatcher.BeginInvoke( (Action) (()=>
      {
        messages.Add(e.Msg);
        ctrlListMessages.ScrollIntoView(messages.Last());
      }));
    }
    private void messageLog_ErrorAdded(SlrrLib.Model.Message e)
    {
      Dispatcher.BeginInvoke( (Action) (()=>
      {
        messages.Add("ERROR!: "+e.Msg);
        ctrlListMessages.ScrollIntoView(messages.Last());
      }));
    }
    private void ctrlListRPK_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Delete)
      {
        if (!currentOperation.IsCompleted)
        {
          MessageBox.Show("A previos operation is underway please wait for it to complete!");
          return;
        }
        foreach (var item in ctrlListRPK.SelectedItems.Cast<object>().ToList())
        {
          rpks.Remove(item as Model.RpkToMerge);
        }
      }
    }
    private void ctrlButtonAddRpkToList_Click(object sender, RoutedEventArgs e)
    {
      if (!currentOperation.IsCompleted)
      {
        MessageBox.Show("A previos operation is underway please wait for it to complete!");
        return;
      }
      System.Windows.Forms.OpenFileDialog diag = new System.Windows.Forms.OpenFileDialog();
      diag.InitialDirectory = LastDirForDialog;
      diag.Multiselect = true;
      diag.Filter = "*.rpk|*.rpk";
      var AddingRpksOperation = new Task(() =>
      {
        foreach (var rpkFnam in diag.FileNames)
        {
          var rpk = gManag.GetRpkFromWeakFileName(rpkFnam);
          if (rpk == null)
            SlrrLib.Model.MessageLog.AddError("The RPK: " + rpkFnam + "\r\nDoes not seem to be in the current Slrr install with Slrr root directory: " + gManag.GetSlrrRoot() + "\r\n it will not be added!");
          else if (rpks.Any(x => x.rpkRepresented == rpk))
            SlrrLib.Model.MessageLog.AddError("The RPK: " + rpkFnam + " is already added the order of the rpks do not matter");
          else
          {
            Dispatcher.Invoke((Action)(() =>
            {
              rpks.Add(new Model.RpkToMerge
              {
                rpkRepresented = gManag.GetRpkFromWeakFileName(rpkFnam)
              });
            }));
          }
        }
      });
      if (diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel && diag.FileNames.Count() != 0)
      {
        LastDirForDialog = System.IO.Path.GetDirectoryName(diag.FileNames.First());
        if (gManag == null)
        {
          string slrRoot = SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(diag.FileNames.First());
          if (slrRoot == "")
          {
            MessageBox.Show("I could not find the Slrr root directory from your selection...\r\nI tried to look for the directory that has cars.rpk maps.rpk and frontedn.rpk in it");
          }
          else
          {
            currentOperation = Task.Factory.StartNew(() =>
            {
              gManag = new SlrrLib.Model.HighLevel.GameFileManager(slrRoot);
              gManag.BuildRpkDict();
            });
            currentOperation.ContinueWith(_ =>
            {
              AddingRpksOperation.Start();
            });
          }
        }
        else
        {
          AddingRpksOperation.Start();
        }
      }
    }
    private void ctrlButtonSelectDestinationRPK_Click(object sender, RoutedEventArgs e)
    {
      if (!currentOperation.IsCompleted)
      {
        MessageBox.Show("A previos operation is underway please wait for it to complete!");
        return;
      }
      if (gManag == null)
      {
        MessageBox.Show("Please select RPKs to merge first");
        return;
      }
      System.Windows.Forms.SaveFileDialog diag = new System.Windows.Forms.SaveFileDialog();
      diag.InitialDirectory = LastDirForDialog;
      diag.Filter = "*.rpk|*.rpk";
      if (diag.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
      {
        merger = new SlrrLib.Model.HighLevel.RpkMerger(diag.FileName, gManag, System.IO.File.Exists(diag.FileName));
        operationCalced = false;
        messages.Add("Selected File for merge result: " + diag.FileName + (System.IO.File.Exists(diag.FileName) ? " This file exists already that is not a problem" : " This file does not exist that is not a problem"));
      }
    }
    private void ctrlButtonCalcOperation_Click(object sender, RoutedEventArgs e)
    {
      if (!currentOperation.IsCompleted)
      {
        MessageBox.Show("A previos operation is underway please wait for it to complete!");
        return;
      }
      if (merger == null)
      {
        MessageBox.Show("Please select a target filename whether an existing or a new RPK file");
        return;
      }
      currentOperation = new Task(() =>
      {
        foreach (var rpk in rpks)
          merger.PushRpk(rpk.rpkRepresented);
        SlrrLib.Model.MessageLog.AddMessage("The following files and folders will be updated if the current operation is run:");
        merger.ReportOperation();
        operationCalced = true;
      });
      currentOperation.Start();
    }
    private void ctrlButtonPreformMerge_Click(object sender, RoutedEventArgs e)
    {
      if (!currentOperation.IsCompleted)
      {
        MessageBox.Show("A previos operation is underway please wait for it to complete!");
        return;
      }
      if (!operationCalced)
      {
        MessageBox.Show("Please calculate operation first");
        return;
      }
      if (MessageBox.Show("Are you sure to merge the RPKs???\r\nThis will not hurt the source rpks BUT!\r\nit will update all the references to the source RPKs (That have been listed after Calculating the operation)\r\nin the game (java,class,other rpks,cfgs)!!!!", "THINK", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
      {
        currentOperation = new Task(() =>
        {
          merger.SaveMerge();
        });
        currentOperation.Start();
      }
    }
  }
}
