﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPKMerger.Model
{
  public class RpkToMerge
  {
    public SlrrLib.Model.HighLevel.RpkManager rpkRepresented;
    public override string ToString()
    {
      if (rpkRepresented == null)
        return "<null>";
      string slrrRoot = SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(rpkRepresented.RpkFileName).ToLower();
      return System.IO.Path.GetFullPath(rpkRepresented.RpkFileName).Remove(0, slrrRoot.Length);
    }
  }
}
