RPKMerger
=======

This app is part of the suite designed for the Street Legal Racing: Redline game (https://en.wikipedia.org/wiki/Street_Legal_Racing:_Redline).
The programs and their development is discussed on the vStanced forums (http://vstanced.com/viewtopic.php?f=68&t=13249).

This is a tool to merge *RPKs* that are on the same level in the *RPK* hierarchy meaning; it will not merge *RPKs* that are dependent on each other.
It will refuse to merge an *RPK* to one that has an external reference to the one being merged or vice-versa.
The *RPKs* and the resources that it references should be installed as they were intended to be used (to the appropriate directories (the mod needs to be installed properly before trying merging)).
It is based on the SlrrLib (https://gitlab.com/slrr/slrr-lib) project.

## Building

You'll need Visual Studio 2019 or higher to build RPKMerger.

1. Clone this repository
2. Open the Visual Studio solution
3. Select either the target platform and build the solution (needed files will be copied over to the target directory).

## Contributing

Any contributions to the project are welcomed, it's recommended to use GitLab [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

## License

All source code in this repository is licensed under a [BSD 3-clause license](LICENSE.md).